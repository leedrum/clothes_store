@extends('client.master')
@section('title')
Sản phẩm
@endsection
@section('content')
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Chi tiết sản phẩm</p>
	</div>
</div>
<div class="container single-product">
	
	<div class="row">
		<div class="col-md-4 product-image">
			<div class="wrapper">
				<img src="{{ asset($product->image) }}">
			</div>
		</div>
		<div class="col-md-8 product-info">
			<h3 class="product-name">{{ $product->name }}</h3>
			<span class="product-code">TS{{ $product->id }}</span>
			<div class="price">
				@if($product->sale != 0)
				<span class="current-price">{{ number_format( ( $product->price * (1- $product->sale/100) ), 0, ',', '.') }}đ</span>
				<span class="old-price"><del>{{ number_format( $product->price, 0, ',', '.' ) }}</del>đ</span>
				@endif

				@if($product->sale == 0)
				<span class="current-price">{{ number_format( $product->price, 0, ',', '.' ) }}đ</span>
				@endif
			</div>
			<p class="short-description">
				{{ $product->short_description }}
			</p>
			<p><i>Tồn kho: {{ $product->quantity }}</i></p>
			<div class="add-to-cart">
				<div class="wrapper">
					<form class="form-inline" action="{{ route('addToCart') }}" method="get">
						<div class="form-group">
							<input type="number" name="quantity" class="form-control" id="quantity" placeholder="Số lượng" value="1" max="{{ $product->quantity }}" min="1">
							<input type="hidden" name="product_id" value="{{ $product->id }}">
						</div>
						<button type="submit" class="btn btn-success btn-add-to-cart">Thêm giỏ hàng</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 description">
			<h3>Mô tả</h3>
			<div class="content">
				{{ $product->description }}
			</div>
		</div>
	</div>
</div>
@endsection