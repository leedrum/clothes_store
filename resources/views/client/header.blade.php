<!DOCTYPE html>
<html>
	<head>
		<title>Tĩn Store - @yield('title')</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg menu-client">
			<a class="navbar-brand" href="/">Tin Store</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/intro">Giới thiệu</a>
					</li>
					
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Sản phẩm
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							@if(count($category_menu[0]) != 0)
								@foreach($category_menu[0] as $category_sanpham)
							<a class="dropdown-item" href="{{ route('showListProduct', ['category_id' => $category_sanpham->id]) }}">{{ $category_sanpham->name }}</a>
								@endforeach
							@endif
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Tin tức
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							@if(count($category_menu[1]) != 0)
								@foreach($category_menu[1] as $category_tintuc)
							<a class="dropdown-item" href="{{ route('showListPost', ['category_id' => $category_tintuc->id]) }}">{{ $category_tintuc->name }}</a>
								@endforeach
							@endif
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('showCart') }}">Giỏ hàng</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Liên hệ</a>
					</li>
					<li class="nav-item dropdown">
						@auth
							<a class="nav-link dropdown-toggle username" href="#" id="logged" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Xin chào {{ \Auth::user()->name }}
							</a>
							<div class="dropdown-menu" aria-labelledby="logged">
								<a class="dropdown-item" href="{{ route('logout') }}"
	                               onclick="event.preventDefault();
	                                             document.getElementById('logout-form').submit();">
	                                {{ __('Đăng xuất') }}
	                            </a>

	                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                @csrf
	                            </form>
							</div>
							
		                @else
						<a class="nav-link dropdown-toggle" href="#" id="login-register" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Đăng nhập/Đăng ký
						</a>
						<div class="dropdown-menu" aria-labelledby="login-register">
							<a class="dropdown-item" href="{{ route('login') }}">Đăng nhập</a>
							<a class="dropdown-item" href="{{ route('register') }}">Đăng ký</a>
						</div>
		                @endauth
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="{{ route('searchProduct') }}" method="get">
					<input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm" name="s" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm kiếm</button>
				</form>

			</div>
		</nav>