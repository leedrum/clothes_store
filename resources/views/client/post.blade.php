@extends('client.master')
@section('title')
Tin tức
@endsection
@section('content')
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">{{ $post->title }}</p>
	</div>
</div>
<div class="container single-post">
	
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				{{ $post->content }}
			</div>
		</div>
	</div>
</div>
@endsection