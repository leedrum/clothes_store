@extends('admin.layouts.master')
@section('title')
Category - Edit
@endsection

@section('content')

<h1><center>Sửa danh mục</center></h1>
<div class="container">
	<form action="/admin/categories/update/{{ $category->id }}" method="post">
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 form-control-label">Tên danh mục</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputEmail3" placeholder="tên danh mục" name="name" value="{{ $category->name }}">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục cha</label>
			<div class="col-sm-10">
				<select class="form-control" name="parent_id">
					<option value="0">--- Chọn ---</option>
					@foreach( $categories as $cat)
					<option value="{{ $cat->id }}" @if($cat->id == $category->parent_id) {{ 'selected' }}  @endif >{{ $cat->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Sửa</button>
			</div>
		</div>

		@csrf
	</form>
</div>
@endsection