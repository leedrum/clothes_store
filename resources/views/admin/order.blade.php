@extends('admin.layouts.master')
@section('title')
Order - List
@endsection

@section('content')
<?php $status = ['Đang chờ', 'Đã thanh toán', 'Đã hủy']; ?>
@if(isset($orders))
<div class="container" style="margin-top: 30px;">

    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    
    
	<table id="orders_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>Mã đơn hàng</th>
                <th>User</th>
                <th>Họ tên người nhận</th>
                <th>Địa chỉ</th>
                <th>Trạng thái</th>
                <th>Tổng số tiền</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($orders))
        	@foreach( $orders as $order)
        	<tr>
                <td>MĐH{{ $order->id }}</td>
                <td>
                    @if($order->user_id == 0)
                    {{ 'Mua tại cửa hàng' }}
                    @endif

                    @if($order->user_id != 0)
                        @foreach($users as $user)
                            @if($user->id == $order->user_id)
                                {{ $user->name }}
                            @endif
                        @endforeach
                    @endif
                </td>
                <td>{{ $order->name }}</td>
                <td>{{ $order->address }}</td>
                <td>{{ $status[$order->status] }}</td>
                <td>{{ number_format( $order->total, 0, ',', '.' ) }}đ</td>
                <td>{{ $order->created_at }}</td>
                <td>{{ $order->updated_at }}</td>
                <td>
                	<a href="{{ route('orders.show', ['id' => $order->id]) }}">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                	<a href="{{ route('deleteOrder', ['id' => $order->id]) }}" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>   
                    </a>
                    <a href="{{ route('printOrder', ['id' => $order->id]) }}" target="_blank">
                        <i class="fa fa-print" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            @endif
    	</tbody>
    	<tfoot>
            <tr>
                <th>Mã đơn hàng</th>
                <th>User</th>
                <th>Họ tên người nhận</th>
                <th>Địa chỉ</th>
                <th>Trạng thái</th>
                <th>Tổng số tiền</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#orders_table').DataTable();
		} );
	</script>

</div>
@endif

@if(isset($update_order))
<br>
<br>
<h1><center>Hóa đơn MHĐ{{ $update_order->id }}</center></h1>
<div class="container">
    <form action="{{ route('orders.store') }}" method="post">
        <div class="form-group row">
            <label for="address" class="col-sm-2 form-control-label">Địa chỉ</label>
            <div class="col-sm-10">
                <span class="form-control">{{ $update_order->address }}</span>
            </div>
        </div>
        <div class="form-group row">
            <label for="user_id" class="col-sm-2 form-control-label">Khách hàng</label>
            <div class="col-sm-10">
                <span class="form-control">
                    @foreach( $users as $user)
                    @if($user->id == $update_order->user_id)
                        {{ $user->name }}
                    @endif
                    @endforeach
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label for="status" class="col-sm-2 form-control-label">Trạng thái</label>
            <div class="col-sm-10">
                <select class="form-control" name="status" id="status">
                    <option value="0" @if($update_order->status == 0) {{ 'selected' }} @endif>Đang chờ</option>
                    <option value="1" @if($update_order->status == 1) {{ 'selected' }} @endif>Đã thanh toán</option>
                    <option value="2" @if($update_order->status == 2) {{ 'selected' }} @endif>Hủy</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea id="description" class="form-control" name="description" placeholder="Ghi chú">{{ $update_order->description }}</textarea>
            </div>
            <input type="hidden" name="id" value="{{ $update_order->id }}">
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Tổng tiền</label>
            <div class="col-sm-10">
                <span class="form-control">{{ number_format( $update_order->total, 0, ',', '.' ) }}đ</span>
            </div>

        </div>

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Sửa</button>
            </div>
        </div>

        @csrf
    </form>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-inverse">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mặt hàng</th>
                            <th>Số lượng</th>
                            <th>Đơn Giá</th>
                            <th>Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt = 1; ?> 
                        @if(!empty($orderItems))
                        @foreach($orderItems as $key => $item)
                        <tr>
                            <td>{{ $stt++ }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ number_format( $item->price, 0, ',', '.' ) }}đ</td>
                            <td>{{ number_format( $item->price * $item->quantity, 0, ',', '.' ) }}đ</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@endif
@endsection