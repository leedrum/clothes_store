@extends('admin.layouts.master')
@section('title')
Post - Edit
@endsection

@section('content')

<h1><center>Sửa bài viết</center></h1>
<div class="container">
	<form action="/admin/posts/update/{{ $post->id }}" method="post">
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Tiêu đề</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" placeholder="tên danh mục" name="title" value="{{ $post->title }}">
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Danh mục</label>
			<div class="col-sm-10">
				<select class="form-control" name="category_id">
					<option value="">--- Chọn ---</option>
					@foreach( $categories as $cat)
					<option value="{{ $cat->id }}" @if($cat->id == $post->category_id) {{ 'selected' }}  @endif >{{ $cat->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Mô tả ngắn</label>
			<div class="col-sm-10">
				<textarea rows="5" class="form-control" name="short_content">{{ $post->short_content }}</textarea>
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Nội dung</label>
			<div class="col-sm-10">
				<textarea rows="10" class="form-control" name="content">{{ $post->content }}</textarea>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Sửa</button>
			</div>
		</div>

		@csrf
	</form>
</div>
@endsection