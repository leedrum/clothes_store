@extends('admin.layouts.master')
@section('title')
Category - List
@endsection

@section('content')
<h1><center>Danh sách</center></h1>
<div class="container">
    
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

	<table id="categories_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Danh mục</th>
                <th>Danh mục cha</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
        	@foreach( $categories as $cat)
        	<tr>
                <td>{{ $cat->id }}</td>
                <td>{{ $cat->name }}</td>
                <td>
                	@foreach($categories as $innerCat)
                	@if($cat->parent_id == $innerCat->id)
                	{{ $innerCat->name }}
                	@endif
                	@endforeach
                </td>
                <td>{{ $cat->created_at }}</td>
                <td>{{ $cat->updated_at }}</td>
                <td>
                    @if($cat->id > 2)
                	<a href="{{ route('categories.show', ['id' => $cat->id]) }}">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                	<a href="{{ route('deleteCategory', ['id' => $cat->id]) }}" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
    	</tbody>
    	<tfoot>
            <tr>
                <th>ID</th>
                <th>Danh mục</th>
                <th>Danh mục cha</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#categories_table').DataTable();
		} );
	</script>
</div>

<br>
<hr>
<br>

<h1><center>Thêm danh mục</center></h1>
<div class="container">
	<form action="/admin/categories/insert" method="post">
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 form-control-label">Tên danh mục</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputEmail3" placeholder="tên danh mục" name="name">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục cha</label>
			<div class="col-sm-10">
				<select class="form-control" name="parent_id">
					<option value="0">--- Chọn ---</option>
					@foreach( $categories as $cat)
					<option value="{{ $cat->id }}">{{ $cat->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Thêm</button>
			</div>
		</div>

		@csrf
	</form>
</div>
@endsection