@extends('admin.layouts.master')
@section('title')
Post - List
@endsection

@section('content')
@if(isset($posts))
<div class="container" style="margin-top: 30px;">

    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    
    
	<table id="post_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
           @foreach($posts as $post)
           <tr>
               <td>{{ $post->id }}</td>
               <td>{{ $post->title }}</td>
               <td>{{ $post->created_at }}</td>
               <td>{{ $post->updated_at }}</td>
               <td>
                    <a href="{{ route('posts.edit', ['id' => $post->id]) }}">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    <a href="{{ route('deletePost', ['id' => $post->id]) }}" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
           </tr>
           @endforeach
    	<tfoot>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#post_table').DataTable();
		} );
	</script>

</div>
@endif

<h1><center>Thêm bài viết</center></h1>
<div class="container">
    <form action="/admin/posts/insert" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 form-control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEmail3" placeholder="Tên sản phẩm" name="title">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục</label>
            <div class="col-sm-10">
                <select class="form-control" name="category_id">
                    <option value="0">Chọn danh mục</option>
                    @foreach( $categories as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="short_description" class="col-sm-2 form-control-label">Mô tả ngắn</label>
            <div class="col-sm-10">
                <textarea rows="5" name="short_description" class="form-control" placeholder="Mô tả ngắn"  id="short_content"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea rows="10" name="content" class="form-control" placeholder="Mô tả"  id="content"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
            </div>
        </div>
        @csrf
    </form>
</div>
@endsection