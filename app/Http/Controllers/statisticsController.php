<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statistic;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $static = new Statistic();
        $data = $static->getStaticByMonth($thang = date('m'), $nam = date('Y'));
        return view('admin.statics', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->by == 'quy') {
            $data = $static->getStaticByQuy( $request->quy, $request->nam );
        }

        if ($request->by == 'thang') {
            $data = $static->getStaticByMonth( $request->thang, $request->nam );
        }

        if ($request->by == 'nam') {
            $data = $static->getStaticByMonth( $request->nam );
        }

        return view('admin.statics', compact($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBy(Request $request)
    {   
        $static = new Statistic();
        if ($request->by == 'q') {
            $data = $static->getStaticByQuy( $request->quy, $request->nam );
        }

        if ($request->by == 't') {
            $data = $static->getStaticByMonth( $request->thang, $request->nam );
        }

        if ($request->by == 'n') {
            $data = $static->getStaticByMonth( $request->nam );
        }
        //dd($data);

        return view('admin.statics', compact('data'));
    }
}
