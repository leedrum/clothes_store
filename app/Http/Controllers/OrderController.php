<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Order;
use App\OrderItem;
use Auth;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $users = User::all();

        return view('admin.order', [ 'orders' => $orders, 'users' => $users ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['description'] = $request->description;
        $data['status'] = $request->status;
        Order::where('id', $request->id)
                ->update($data);

        return redirect('admin/orders')->with('success', 'Sửa thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::all();
        $update_order = Order::find($id);
        $order_item = new OrderItem();
        $orderItems = $order_item->getAll($id);

        return view('admin.order', compact('users', 'update_order', 'orderItems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function insert(Request $request) 
    {

        /**
         * insert to order table
         */
        $order = new Order();
        $order->user_id = Auth::id();
        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->address = $request->address;
        $order->description = $request->description;
        $order->total = $request->total;
        $order->status = 0;
        $order->save();



        /**
         * insert order items
         */

        $cart = $_SESSION['cart'];

        if(!empty($cart)) {
            foreach ($cart as $product_id => $quantity) {
                $product = Product::find($product_id);

                $orderitem =  new OrderItem();

                $orderitem->product_id = $product_id;
                $orderitem->quantity = $quantity;
                $orderitem->order_id = $order->id;
                $orderitem->price = ($product->price * (1- $product->sale /100));

                $orderitem->save();

                Product::where('id', $product_id    )
                    ->update(['quantity' => ($product->quantity - $quantity)]);
            }
        }

        unset($_SESSION['cart']);

        return redirect('checkoutSuccess')->with('success', 'Thêm thành công');
    }

    public function delete($id)
    {
        $order = new Order();
        $order = Order::find($id);
        $order->delete();
        return redirect('admin/orders')->with('success', 'Xóa thành công');
    }

    public function showCheckout() 
    {
        if(Auth::check()) {
            $products = [];
            $totalMoney = 0;
            if (isset($_SESSION['cart'])) {
                foreach ($_SESSION['cart'] as $product_id => $quantity) {
                    $product = Product::find($product_id)->toArray();
                    $product['cart_quantity'] = $quantity;
                    $products[] = $product;
                
                    $totalMoney += $product['price'] * ( 1- $product['sale']/100) * $quantity;
                }
            }

            return view('client.checkout', ['products' => $products, 'totalMoney' => $totalMoney]);
        } else {

            return redirect('login');
        }
        
    }


    public function printOrder($id) 
    {   
        $order = new Order();
        $order = Order::find($id);

        $orderitem = new OrderItem();

        $orderItems = $orderitem->getAll($id)->toArray();
        
        return view('admin.bill', ['order' => $order, 'orderItems' => $orderItems]);
    }

    public function checkoutSuccess()
    {
        return view('client.checkoutSuccess');
    }
}
