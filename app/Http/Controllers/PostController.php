<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        $categories = Category::where('parent_id', '=', '2')->get();

        return view('admin.post', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::all();

        return view('admin.showPost', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        unset($data['_token']);
        Post::where('id', $id)
                ->update($data);

        return redirect('admin/posts')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showList($category_id)
    {
        $posts = Post::where('category_id', '=', $category_id)->get();
        $title_category = Category::find($category_id)->name;
        return view('client.listPost', compact('posts', 'title_category'));
    }

    public function showSinglePost($id)
    {
        $post = Post::findOrFail($id);
        if($post != false){
            return view('client.post', compact('post'));
        } else {
            return abort('404');
        }
        
    }

    public function insert(Request $request)
    {
        $product = new Post();
        $product->create($request->all());

        return redirect('admin/posts')->with('success', 'Thêm bài viết thành công');
    }

    public function delete($id)
    {
        $post = new Post();
        $post = Post::findOrFail($id);
        $post->delete();
        
        return redirect('admin/posts')->with('success', 'Xóa thành công');
    }
}
