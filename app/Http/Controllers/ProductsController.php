<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Http\UploadedFile;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();

        return view('admin.product', ['products' => $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $updateView = $product->view;
        $updateView++;
        Product::where('id', $id)
                ->update(['view' => $updateView]);

        if ($product) {
            return view('client.product', [ 'product' => $product ]);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();

        return view('admin.showProduct', ['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_product = Product::find($id);
        $data = $request->all();
        unset($data['_token']);

        if (isset($data['image'])) {
            $data['image'] = $this->upload($request->file('image'), 'storage/products/');
        } else {
            $data['image'] = $old_product->image;
        }

        Product::where('id', $id)
                ->update($data);

        return redirect('admin/products')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
    }


    public function insert(Request $request)
    {
        $data = $request->all();
        $image = $this->upload($request->file('image'), 'storage/products/');
        $data['image'] = $image;
        $product = new Product();
        $product->create($data);

        return redirect('admin/products')->with('success', 'Thêm sản phẩm thành công');
            
    }

    public function upload($file, $path)
    {
        $name = $file->getClientOriginalName();
        $file->move($path, $name);

        return $path . $name;
    }

    public function delete($id)
    {
        $cat = new Product();
        $cat = Product::find($id);
        $cat->delete();
        return redirect('admin/products')->with('success', 'Xóa thành công');
    }


    public function search()
    {   $s = $_GET['s'];

        $product = new Product();
        $products = $product->getProductByKeyWord($s);
        $total_search = count($products);

        return view('client.shop', compact('products', 'total_search'));
    }


    public function productOrder(Request $request)
    {   

        if($request->orderBy != 'price' && $request->orderBy != 'sex')
            return abort(404);

        $product = new Product();

        if($request->orderBy == 'price') {

            $orderBy = $request->orderBy;
            $order = $request->order;
            $products = $product->productOrder($request->orderBy, $request->order);

        } else {

            if($request->orderBy == 'sex') {

                $products = Product::where('sex', '=', $request->order)
                                ->get();

            } else {
                return abort(404);
            }

        }
        return view('client.shop', compact('products'));
    }

    public function showList($category_id)
    {
        $products = Product::where('category_id', '=', $category_id)->get();
        $title_category = Category::find($category_id)->name;
        return view('client.shop', compact('products', 'title_category'));
    }
}
