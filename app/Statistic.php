<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Product;
use App\Category;
use App\Order;
use App\OrderItem;

class Statistic extends Model
{
    public function getStaticByQuy( $quy, $nam )
    {
    	if($nam == '') $nam = date('Y');
    	if($quy == '') $quy = 1;
    	$data = [];
    	$order = new Order();
    	$product = new Product();
    	$OrderItem = new OrderItem();
    	$orderMax = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
    			->whereMonth('created_at', '>=', ($quy*3 - 3))
    			->whereMonth('created_at', '<', (int)$quy * 3 )
				->orderBy('total', 'desc')
    			->get()->toArray();
    	if(isset($orderMax[0]))
    	$data['orderMax'] = $orderMax[0];

    	$orderMin = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
    			->whereMonth('created_at', '>=', ($quy*3 - 3))
    			->whereMonth('created_at', '<', (int)$quy * 3 )
				->orderBy('total', 'asc')
    			->get()->toArray();
		if(isset($orderMin[0]))
    	$data['orderMin'] = $orderMin[0];

    	$productView = DB::table('products')
    			->orderBy('view', 'desc')
    			->get()->toArray();
		if(isset($productView[0]))
    	$data['productView'] = $productView[0];

    	$productBestSell = DB::table('order_items')->select('product_id')->max('quantity');
        if (!empty($productBestSell)) {
            $data['productBestSell'] = Product::find($productBestSell)->toArray();
        }
    	

    	$totalCountOrder = DB::table('orders')->count();
    	$data['totalCountOrder'] = $totalCountOrder;

    	$totalPriceOrder = DB::table('orders')->sum('total');
    	$data['totalPriceOrder'] = (int)$totalPriceOrder;

    	return $data;
    }

    public function getStaticByMonth( $month, $nam )
    {
    	if($nam == '') $nam = date('Y');
    	$data = [];
    	$order = new Order();
    	$product = new Product();
    	$OrderItem = new OrderItem();
    	$orderMax = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
    			->whereMonth('created_at', '=', $month)
				->orderBy('total', 'desc')
    			->get()->toArray();
    	if(isset($orderMax[0]))
    	$data['orderMax'] = $orderMax[0];

    	$orderMin = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
    			->whereMonth('created_at', '=', $month)
				->orderBy('total', 'asc')
    			->get()->toArray();
		if(isset($orderMin[0]))
    	$data['orderMin'] = $orderMin[0];

    	$productView = DB::table('products')
    			->orderBy('view', 'desc')
    			->get()->toArray();
		if(isset($productView[0]))
    	$data['productView'] = $productView[0];

    	$productBestSell = DB::table('order_items')->select('product_id')->max('quantity');
    	if (!empty($productBestSell)) {
            $data['productBestSell'] = Product::find($productBestSell)->toArray();
        }

    	$totalCountOrder = DB::table('orders')->count();
    	$data['totalCountOrder'] = $totalCountOrder;

    	$totalPriceOrder = DB::table('orders')->sum('total');
    	$data['totalPriceOrder'] = (int)$totalPriceOrder;

    	return $data;
    }

    public function getStaticByYear( $nam )
    {
    	if($nam == '') $nam = date('Y');
    	$data = [];
    	$order = new Order();
    	$product = new Product();
    	$OrderItem = new OrderItem();
    	$orderMax = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
				->orderBy('total', 'desc')
    			->get()->toArray();
    	if(isset($orderMax[0]))
    	$data['orderMax'] = $orderMax[0];

    	$orderMin = DB::table('orders')
    			->whereYear('created_at', '>=', $nam)
				->orderBy('total', 'asc')
    			->get()->toArray();
		if(isset($orderMin[0]))
    	$data['orderMin'] = $orderMin[0];

    	$productView = DB::table('products')
    			->orderBy('view', 'desc')
    			->get()->toArray();
		if(isset($productView[0]))
    	$data['productView'] = $productView[0];

    	$productBestSell = DB::table('order_items')->select('product_id')->max('quantity');
    	if (!empty($productBestSell)) {
            $data['productBestSell'] = Product::find($productBestSell)->toArray();
        }

    	

    	$totalCountOrder = DB::table('orders')->count();
    	$data['totalCountOrder'] = $totalCountOrder;

    	$totalPriceOrder = DB::table('orders')->sum('total');
    	$data['totalPriceOrder'] = (int)$totalPriceOrder;

    	return $data;
    }
}
