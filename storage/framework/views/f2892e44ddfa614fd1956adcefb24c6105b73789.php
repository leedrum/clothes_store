<?php $__env->startSection('title'); ?>
Thanh toán
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Thanh toán</p>
	</div>
</div>
<div class="container checkout">
	<div class="row">
		<form action="<?php echo e(route('insertOrder')); ?>" method="POST" class="col-md-12">
			<div class="flex-1 info-customer">
				<h3 class="title-checkout">Thông tin khách hàng</h3>
				<div class="form-group">
					<label>Họ tên người nhận</label>
					<input type="text" name="name" class="form-control" value="<?php echo e(\Auth::user()->name); ?>">
				</div>
				<div class="form-group">
					<label>Số điện thoại</label>
					<input type="text" name="phone" class="form-control">
				</div>
				<div class="form-group">
					<label>Địa chỉ</label>
					<input type="text" name="address" class="form-control">
				</div>
				<div class="form-group">
					<label>Ghi chú</label>
					<textarea name="description" class="form-control"></textarea>
				</div>
			</div>
			<div class="flex-1 info-cart">
				<h3 class="title-checkout">Thông tin đơn hàng</h3>
				<table class="table table-inverse text-center">
					<thead>
						<tr>
							<th>Sản phẩm</th>
							<th>Số lượng</th>
							<th>Thành tiền</th>
						</tr>
					</thead>
					<tbody>
						<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($product['name']); ?></td>
							<td>x<?php echo e($product['cart_quantity']); ?></td>
							<td>
								<?php echo e(number_format($product['price'] * ( 1- $product['sale']/100) * $product['cart_quantity'], 0, ',', '.')); ?>đ
							</td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<tr class="total">
							<td></td>
							<td class="text">Tổng số tiền</td>
							<td class="price"><?php echo e(number_format($totalMoney, 0, ',', '.')); ?>đ</td>
							<input type="hidden" name="total" value="<?php echo e($totalMoney); ?>">
						</tr>
						<tr>
							<td></td>
							<td><?php echo csrf_field(); ?></td>
							<td><button type="submit" class="btn btn-success">GỬI ĐƠN HÀNG</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>