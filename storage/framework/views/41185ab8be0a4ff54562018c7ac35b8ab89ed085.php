<?php $__env->startSection('title'); ?>
Product - List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container" style="margin-top: 30px;">

    <?php if(session()->has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success')); ?>

        </div>
    <?php endif; ?>

    <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('error')); ?>

        </div>
    <?php endif; ?>

	<table id="products_table" class=" table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Sản phẩm</th>
                <th>Danh mục</th>
                <th>Ảnh</th>
                <th>Giá</th>
                <th>Số lượng</th>
                <th>Giảm giá</th>
                <th>Giới tính</th>
                <th>Lượt xem</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
        	<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        	<tr>
                <td><?php echo e($product->id); ?></td>
                <td><?php echo e($product->name); ?></td>
                <td>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($product->category_id == $cat->id): ?>
                            <?php echo e($cat->name); ?>

                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </td>
                <td><img src="<?php echo e(asset($product->image)); ?>" style="max-width: 150px;"></td>
                <td><?php echo e($product->price); ?></td>
                <td><?php echo e($product->quantity); ?></td>
                <td><?php echo e($product->sale); ?>%</td>
                <td><?php echo e($product->sex == 1 ? 'nam' : 'nữ'); ?></td>
                <td><?php echo e($product->view); ?></td>
                <td><?php echo e($product->created_at); ?></td>
                <td><?php echo e($product->updated_at); ?></td>
                <td>
                	<a href="<?php echo e(route('products.edit', ['id' => $product->id])); ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                	<a href="<?php echo e(route('deleteProduct', ['id' => $product->id])); ?>" class="confirm">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	</tbody>
    	<tfoot>
            <tr>
                <th>ID</th>
                <th>Sản phẩm</th>
                <th>Danh mục</th>
                <th>Ảnh</th>
                <th>Giá</th>
                <th>Số lượng</th>
                <th>Giảm giá</th>
                <th>Giới tính</th>
                <th>Lượt xem</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#products_table').DataTable();
		} );
	</script>
</div>

<br>
<hr>
<br>

<h1><center>Thêm sản phẩm</center></h1>
<div class="container">
    <form action="/admin/products/insert" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 form-control-label">Tên sản phẩm</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEmail3" placeholder="Tên sản phẩm" name="name">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục</label>
            <div class="col-sm-10">
                <select class="form-control" name="category_id">
                    <option value="0">Chọn danh mục</option>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="quantity" class="col-sm-2 form-control-label">Số lượng</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="quantity" placeholder="Số lượng" name="quantity">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 form-control-label">Giá</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="price" placeholder="Giá" name="price">
            </div>
        </div>
        <div class="form-group row">
            <label for="sale" class="col-sm-2 form-control-label">Giảm giá</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="sale" placeholder="Giảm giá" name="sale">
            </div>
        </div>
        <div class="form-group row">
            <label for="sex" class="col-sm-2 form-control-label">Giới tính</label>
            <div class="col-sm-10">
                <select class="form-control" name="sex">
                    <option value="1">Nam</option>
                    <option value="0">Nữ</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="short_description" class="col-sm-2 form-control-label">Mô tả ngắn</label>
            <div class="col-sm-10">
                <textarea name="short_description" class="form-control" placeholder="Mô tả ngắn"  id="short_description" required="required"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="description" class="form-control" placeholder="Mô tả"  id="description" required="required"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-sm-2 form-control-label">Ảnh</label>
            <div class="col-sm-10">
                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="fileHelp" required="required">
                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
            </div>
        </div>
        <?php echo csrf_field(); ?>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>