<?php $__env->startSection('title'); ?>
Product - Edit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1><center>Sửa sản phẩm</center></h1>
<div class="container">
    <form action="/admin/products/update/<?php echo e($product->id); ?>" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="name" class="col-sm-2 form-control-label">Tên sản phẩm</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" value="<?php echo e($product->name); ?>" placeholder="Tên sản phẩm" name="name">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục</label>
            <div class="col-sm-10">
                <select class="form-control" name="category_id">
                    <option value="0">Chọn danh mục</option>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($cat->id); ?>" <?php if($product->category_id == $cat->id): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($cat->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="quantity" class="col-sm-2 form-control-label">Số lượng</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" value="<?php echo e($product->quantity); ?>" id="quantity" placeholder="Số lượng" name="quantity">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 form-control-label">Giá</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" value="<?php echo e($product->price); ?>" id="price" placeholder="Giá" name="price">
            </div>
        </div>
        <div class="form-group row">
            <label for="sale" class="col-sm-2 form-control-label">Giảm giá</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" value="<?php echo e($product->sale); ?>" id="sale" placeholder="Giảm giá" name="sale">
            </div>
        </div>
        <div class="form-group row">
            <label for="sex" class="col-sm-2 form-control-label">Giới tính</label>
            <div class="col-sm-10">
                <select class="form-control" name="sex">
                    <option value="">Chọn giới tính</option>
                    <option value="1"<?php echo e($product->sex == 1 ? 'selected' : ''); ?>>Nam</option>
                    <option value="0">Nữ</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="short_description" class="col-sm-2 form-control-label">Mô tả ngắn</label>
            <div class="col-sm-10">
                <textarea name="short_description" class="form-control" placeholder="Mô tả ngắn"  id="short_description"><?php echo e($product->short_description); ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="description" class="form-control" placeholder="Mô tả"  id="description"><?php echo e($product->description); ?></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-sm-2 form-control-label">Ảnh</label>
            <div class="col-sm-10">
                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="fileHelp">
                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
            </div>
        </div>
        <img src="<?php echo e(asset($product->image)); ?>">

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Sửa</button>
            </div>
        </div>
        <?php echo csrf_field(); ?>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>