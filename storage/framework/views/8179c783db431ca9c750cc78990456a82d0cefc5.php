<?php $__env->startSection('title'); ?>
Post - List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(isset($posts)): ?>
<div class="container" style="margin-top: 30px;">

    <?php if(session()->has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success')); ?>

        </div>
    <?php endif; ?>

    <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('error')); ?>

        </div>
    <?php endif; ?>
    
    
	<table id="post_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
           <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           <tr>
               <td><?php echo e($post->id); ?></td>
               <td><?php echo e($post->title); ?></td>
               <td><?php echo e($post->created_at); ?></td>
               <td><?php echo e($post->updated_at); ?></td>
               <td>
                    <a href="<?php echo e(route('posts.edit', ['id' => $post->id])); ?>">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    <a href="<?php echo e(route('deletePost', ['id' => $post->id])); ?>" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
           </tr>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	<tfoot>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#post_table').DataTable();
		} );
	</script>

</div>
<?php endif; ?>

<h1><center>Thêm bài viết</center></h1>
<div class="container">
    <form action="/admin/posts/insert" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 form-control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEmail3" placeholder="Tên sản phẩm" name="title">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục</label>
            <div class="col-sm-10">
                <select class="form-control" name="category_id">
                    <option value="0">Chọn danh mục</option>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="short_description" class="col-sm-2 form-control-label">Mô tả ngắn</label>
            <div class="col-sm-10">
                <textarea rows="5" name="short_description" class="form-control" placeholder="Mô tả ngắn"  id="short_content"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea rows="10" name="content" class="form-control" placeholder="Mô tả"  id="content"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
            </div>
        </div>
        <?php echo csrf_field(); ?>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>