<!DOCTYPE html>
<html>
<head>
	<title>Hóa đơn</title>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/app.css')); ?>">
</head>
<body>
	<div class="container content-print-order" style="margin-top: 30px;">
		<div class="row">
			<div class="col-md-6">
				<h3>Tin Store</h3>
				<p>Địa chỉ: Tầng 18 tòa nhà ABC An Hòa Hà Đông, Hà Nội</p>
				<p>Hotline: 0123456789</p>
				<p>Email: TinStore&#64;gmail.com</p>
				<p>Website: tinstore.com</p>
			</div>
			<div class="col-md-6">
				<h3>HÓA ĐƠN BÁN HÀNG</h3>
				<?php if(!empty($order)): ?>
				<p>Mã đơn hàng: MĐH<?php echo e($order->id); ?></p>
				<p>Khách hàng: <?php echo e($order->name); ?></p>
				<p>Số điện thoại: <?php echo e($order->phone); ?></p>
				<p>Địa chỉ: <?php echo e($order->address); ?></p>
				<p>Ghi chú: <?php echo e($order->description); ?></p>
				<?php endif; ?>
			</div>
		</div>

		<div class="row">
			<table class="table table-inverse table-bordered text-center">
				<thead>
					<tr>
						<th>STT</th>
						<th>Mặt hàng</th>
						<th>Số lượng</th>
						<th>Đơn Giá</th>
						<th>Thành tiền</th>
					</tr>
				</thead>
				<tbody>
					<?php $stt = 1; ?> 
					<?php if(!empty($orderItems)): ?>
					<?php $__currentLoopData = $orderItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><?php echo e($stt++); ?></td>
						<td><?php echo e($item->name); ?></td>
						<td><?php echo e($item->quantity); ?></td>
						<td><?php echo e(number_format( $item->price, 0, ',', '.' )); ?>đ</td>
						<td><?php echo e(number_format( $item->price * $item->quantity, 0, ',', '.' )); ?>đ</td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</tbody>
			</table>

		</div>

		<div class="row" style="margin-top: 30px;">
			<div class="col-md-4">
				<p class="text-center">Người lập hóa đơn</p>
			</div>
			<div class="col-md-8 text-center">
				<p class="">Tổng cộng: <?php echo e(number_format( $order->total, 0, ',', '.' )); ?>đ</p>
				<p class="">Ngày ... Tháng ... Năm ...</p>
			</div>
		</div>
	</div>
</body>
</html>