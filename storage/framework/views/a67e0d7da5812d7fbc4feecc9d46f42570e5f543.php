<?php $__env->startSection('title'); ?>
Statics
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container" style="margin-top: 40px;">
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-md-12">
                <h3>Xu hướng</h3>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="xu_huong">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Sản phẩm nhiều lượt xem nhất: </td>
                                <td>
                                    <?php if(isset($data['productView'])): ?>
                                    <a href="<?php echo e(route('products.edit', ['id' => $data['productView']->id])); ?>"><?php echo e($data['productView']->name); ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Sản phẩm bán chạy nhất: </td>
                                <td>
                                    <?php if(isset($data['productBestSell'])): ?>
                                    <a href="<?php echo e(route('products.edit', ['id' => $data['productBestSell']['id']])); ?>"><?php echo e($data['productBestSell']['name']); ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row" style="margin-bottom: 25px;">
            <div class="col-md-12">
                <h3>Thống kê đơn hàng</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="Thu nhập">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Đơn hàng lớn nhất: </td>
                                <td>
                                    <?php if(isset($data['orderMax'])): ?>
                                    <a href="<?php echo e(route('orders.show', ['id' => $data['orderMax']->id])); ?>">MĐH<?php echo e($data['orderMax']->id); ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Đơn hàng nhỏ nhất: </td>
                                <td>
                                    <?php if(isset($data['orderMin'])): ?>
                                    <a href="<?php echo e(route('orders.show', ['id' => $data['orderMin']->id])); ?>">MĐH<?php echo e($data['orderMin']->id); ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Tổng số đơn hàng </td>
                                <td><?php echo e($data['totalCountOrder']); ?></td>
                            </tr>
                            <tr>
                                <td>Tổng số tiền các đơn hàng </td>
                                <td><?php echo e(number_format($data['totalPriceOrder'], 0, ',', '.')); ?>đ</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <h3>Xem thống kê</h3>
                <form action="<?php echo e(route('getByStatistics')); ?>" method="post">
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tháng</label>
                        <div class="col-sm-10">
                            <input type="number" min="1" max="12" class="form-control" name="thang">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Quý</label>
                        <div class="col-sm-10">
                            <input type="number" min="1" max="4" class="form-control" name="quy">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Năm</label>
                        <div class="col-sm-10">
                            <input type="number" min="1990" max="2099" class="form-control" name="nam">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Thống kê theo</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="by">
                                <option value="t">Tháng</option>
                                <option value="q">Quý</option>
                                <option value="n">Năm</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Xem</button>
                        </div>
                    </div>

                    <?php echo csrf_field(); ?>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>