<?php $__env->startSection('title'); ?>
Category - Edit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1><center>Sửa danh mục</center></h1>
<div class="container">
	<form action="/admin/categories/update/<?php echo e($category->id); ?>" method="post">
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 form-control-label">Tên danh mục</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputEmail3" placeholder="tên danh mục" name="name" value="<?php echo e($category->name); ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục cha</label>
			<div class="col-sm-10">
				<select class="form-control" name="parent_id">
					<option value="0">--- Chọn ---</option>
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($cat->id); ?>" <?php if($cat->id == $category->parent_id): ?> <?php echo e('selected'); ?>  <?php endif; ?> ><?php echo e($cat->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Sửa</button>
			</div>
		</div>

		<?php echo csrf_field(); ?>
	</form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>