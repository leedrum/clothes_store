<?php $__env->startSection('title'); ?>
Order - List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php $status = ['Đang chờ', 'Đã thanh toán', 'Đã hủy']; ?>
<?php if(isset($orders)): ?>
<div class="container" style="margin-top: 30px;">

    <?php if(session()->has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success')); ?>

        </div>
    <?php endif; ?>

    <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('error')); ?>

        </div>
    <?php endif; ?>
    
    
	<table id="orders_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>Mã đơn hàng</th>
                <th>User</th>
                <th>Họ tên người nhận</th>
                <th>Địa chỉ</th>
                <th>Trạng thái</th>
                <th>Tổng số tiền</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($orders)): ?>
        	<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        	<tr>
                <td>MĐH<?php echo e($order->id); ?></td>
                <td>
                    <?php if($order->user_id == 0): ?>
                    <?php echo e('Mua tại cửa hàng'); ?>

                    <?php endif; ?>

                    <?php if($order->user_id != 0): ?>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($user->id == $order->user_id): ?>
                                <?php echo e($user->name); ?>

                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </td>
                <td><?php echo e($order->name); ?></td>
                <td><?php echo e($order->address); ?></td>
                <td><?php echo e($status[$order->status]); ?></td>
                <td><?php echo e(number_format( $order->total, 0, ',', '.' )); ?>đ</td>
                <td><?php echo e($order->created_at); ?></td>
                <td><?php echo e($order->updated_at); ?></td>
                <td>
                	<a href="<?php echo e(route('orders.show', ['id' => $order->id])); ?>">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                	<a href="<?php echo e(route('deleteOrder', ['id' => $order->id])); ?>" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>   
                    </a>
                    <a href="<?php echo e(route('printOrder', ['id' => $order->id])); ?>" target="_blank">
                        <i class="fa fa-print" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
    	</tbody>
    	<tfoot>
            <tr>
                <th>Mã đơn hàng</th>
                <th>User</th>
                <th>Họ tên người nhận</th>
                <th>Địa chỉ</th>
                <th>Trạng thái</th>
                <th>Tổng số tiền</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#orders_table').DataTable();
		} );
	</script>

</div>
<?php endif; ?>

<?php if(isset($update_order)): ?>
<br>
<br>
<h1><center>Hóa đơn MHĐ<?php echo e($update_order->id); ?></center></h1>
<div class="container">
    <form action="<?php echo e(route('orders.store')); ?>" method="post">
        <div class="form-group row">
            <label for="address" class="col-sm-2 form-control-label">Địa chỉ</label>
            <div class="col-sm-10">
                <span class="form-control"><?php echo e($update_order->address); ?></span>
            </div>
        </div>
        <div class="form-group row">
            <label for="user_id" class="col-sm-2 form-control-label">Khách hàng</label>
            <div class="col-sm-10">
                <span class="form-control">
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($user->id == $update_order->user_id): ?>
                        <?php echo e($user->name); ?>

                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </span>
            </div>
        </div>

        <div class="form-group row">
            <label for="status" class="col-sm-2 form-control-label">Trạng thái</label>
            <div class="col-sm-10">
                <select class="form-control" name="status" id="status">
                    <option value="0" <?php if($update_order->status == 0): ?> <?php echo e('selected'); ?> <?php endif; ?>>Đang chờ</option>
                    <option value="1" <?php if($update_order->status == 1): ?> <?php echo e('selected'); ?> <?php endif; ?>>Đã thanh toán</option>
                    <option value="2" <?php if($update_order->status == 2): ?> <?php echo e('selected'); ?> <?php endif; ?>>Hủy</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-2 form-control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea id="description" class="form-control" name="description" placeholder="Ghi chú"><?php echo e($update_order->description); ?></textarea>
            </div>
            <input type="hidden" name="id" value="<?php echo e($update_order->id); ?>">
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Tổng tiền</label>
            <div class="col-sm-10">
                <span class="form-control"><?php echo e(number_format( $update_order->total, 0, ',', '.' )); ?>đ</span>
            </div>

        </div>

        <div class="form-group row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Sửa</button>
            </div>
        </div>

        <?php echo csrf_field(); ?>
    </form>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-inverse">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mặt hàng</th>
                            <th>Số lượng</th>
                            <th>Đơn Giá</th>
                            <th>Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt = 1; ?> 
                        <?php if(!empty($orderItems)): ?>
                        <?php $__currentLoopData = $orderItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($stt++); ?></td>
                            <td><?php echo e($item->name); ?></td>
                            <td><?php echo e($item->quantity); ?></td>
                            <td><?php echo e(number_format( $item->price, 0, ',', '.' )); ?>đ</td>
                            <td><?php echo e(number_format( $item->price * $item->quantity, 0, ',', '.' )); ?>đ</td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>