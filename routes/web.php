<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/intro', 'HomeController@intro')->name('intro');

Route::get('admin', function (){
	if (Auth::user() == NULL) {
		return redirect('login');
	} else {
		if(Auth::user()->permission != 0) {
			return abort('404');
		}
	}
	return view('admin.dashboard');
	
})->name('adminDashboard');

Route::resource('admin/categories', 'CategoriesController');

Route::post('admin/categories/insert', 'CategoriesController@insert')->name('insertCategory');

Route::post('admin/categories/update/{id}', 'CategoriesController@update')->name('updateCategory');

Route::get('admin/categories/delete/{id}', 'CategoriesController@delete')->name('deleteCategory');

Route::resource('admin/products', 'ProductsController');

Route::post('admin/products/insert', 'ProductsController@insert')->name('insertProduct');

Route::post('admin/products/update/{id}', 'ProductsController@update')->name('updateProduct');

Route::get('admin/products/delete/{id}', 'ProductsController@delete')->name('deleteProduct');

Route::get('categoryProduct/{category_id}', 'ProductsController@showList')->name('showListProduct');

Route::resource('admin/orders', 'OrderController');

Route::post('admin/orders/insert', 'OrderController@insert')->name('insertOrder');

Route::post('admin/orders/update/{id}', 'OrderController@update')->name('updateOrder');

Route::get('admin/orders/delete/{id}', 'OrderController@delete')->name('deleteOrder');

Route::get('admin/printOrder/{id}', 'OrderController@printOrder')->name('printOrder');

Route::get('admin/statistics', 'StatisticsController@index')->name('indexStatistics');

Route::post('admin/statistics/getBy', 'StatisticsController@getBy')->name('getByStatistics');

Route::resource('admin/posts', 'PostController');

Route::post('admin/posts/insert', 'PostController@insert')->name('insertPost');

Route::post('admin/posts/update/{id}', 'PostController@update')->name('updatePost');

Route::get('admin/posts/delete/{id}', 'PostController@delete')->name('deletePost');

Route::get('categoryPost/{category_id}', 'PostController@showList')->name('showListPost');

Route::get('post/{id}', 'PostController@showSinglePost')->name('showSinglePost');

//// Clients

Route::get('products/{id}', 'ProductsController@show')->name('showProduct');

Route::get('cart', 'CartController@showCart')->name('showCart');

Route::get('addToCart', 'CartController@addToCart')->name('addToCart');

Route::post('updateCart', 'CartController@updateCart')->name('updateCart');

Route::get('emptyCart', 'CartController@emptyCart')->name('emptyCart');

Route::get('checkout', 'OrderController@showCheckout')->name('showCheckout');

Route::get('checkoutSuccess', 'OrderController@checkoutSuccess')->name('checkoutSuccess');

Route::get('search', 'ProductsController@search')->name('searchProduct');

Route::get('productOrder/{orderBy}/{order}', 'ProductsController@productOrder')->name('productOrder');