<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'user_id' => mt_rand(1, 10)(10),
            'address' => str_random(50),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
